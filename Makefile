# @Makefile for barcode-imag
# @author VIZCREATIONS
# @copyright (C) 2014

SRC_CIL = barc128.c barc39.c
OBJ_CIL = barc128.o barc39.o

CIL_INCLUDES = -I/usr/include -I. -I/usr/local/include
CIL_LIBS = -L/usr/lib -L/usr/local/lib -lgd -lc

all: cil default
cil:
	gcc -c $(SRC_CIL)
	ar rcs bimag.a $(OBJ_CIL)
	$(RM) *.o

default:
	gcc -o barc $(CIL_INCLUDES) main.c $(CIL_LIBS)
install:
	cp barc /usr/local/bin
clean:
	$(RM) barc *.a *.o tmp/* images/*
