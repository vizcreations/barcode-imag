/**
* @barc39 source file for barcode-imag library
* @author VIZCREATIONS
* @copyright (C) 2014
*/

#include <gd.h>
#include <gdfonts.h>
#include "bimag.h"

char *bars = "";
char *encode_barc39(BIM *bi, char *barnum, int check_digit) { // Expecting barnum to be larger than chars it holds..
	/** Encode table with  values */
	char enc_tablev[] = {
				"NNNWWNWNN", // 0
				"WNNWNNNNW",
				"NNWWNNNNW",
				"WNWWNNNNN",
				"NNNWWNNNW", // 4
				"WNNWWNNNN",
				"NNWWWNNNN",
				"NNNWNNWNW", // 7
				"WNNWNNWNN",
				"NNWWNNWNN",
				"NNWWNNWNN", // A
				"NNWNNWNNW",
				"WNWNNWNNN",
				"NNNNWWNNW", // D
				"WNNNWWNNN",
				"NNWNWWNNN",
				"NNNNNWWNW", // G
				"WNNNNWWNN",
				"NNWNNWWNN",
				"NNNNWWWNN",
				"WNNNNNNWW", // K
				"NNWNNNNWW",
				"WNWNNNNWN",
				"NNNNWNNWW",
				"WNNNWNNWN", // O
				"NNWNWNNWN",
				"NNNNNNWWW",
				"WNNNNNWWN",
				"NNWNNNWWN", // S
				"NNNNWNWWN",
				"WWNNNNNNW",
				"NWWNNNNNW",
				"WWWNNNNNN",
				"NWNNWNNNW", // X
				"WWNNWNNNN",
				"NWWNWNNNN",
				"NWNNNNWNW", // -
				"WWNNNNWNN", // .
				"NWWNNNWNN", // space
				"NWNWNWNNN", // $
				"NWNWNNNWN", // [/]
				"NWNNNWNWN", // +
				"NNNWNWNWN", // %
				"NWNNWNWNN" // *
			};

	/** Encode table with keys.. */
	char enc_tablevk = {
				"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z",
				"-", ".", " ", "$", "/", "+", "%", "*"
			};

	char mfc_str[BIM_MAX_INPUT];
	char *widebar;
	char *widespc;
	int i, j, len, slen;
	char num;
	int snum = 0;
	int tot = 0;

	char barnumn[len*2];
	char *value;
	int bar = FALSE;

	len = strlen(barnum);
	if(check_digit == TRUE) {
		for(i=0; i<len; i++) {
			num = barnum[i];

			if(preg_match("/[A-Z]+/",num))
				snum = ord(num)-55; // Possibly ord returns integer..
			else if(num=='-')
				snum = 36;
			else if(num=='.')
				snum = 37;
			else if(isspace(num))
				snum = 38;
			else if(num=='$')
				snum = 39;
			else if(num=='/')
				snum = 40;
			else if(num=='+')
				snum = 41;
			else if(num=='%')
				snum = 42;
			else if(num=='*')
				snum = 43;

			tot += snum;
		}

		strcat(barnum, trim(enc_tablek[(int)tot%43]));
	}

	memset(barnumn, 0, sizeof(barnumn));
	sprintf(barnumn, "%c%s%c", '*', barnum, '*');

	i=0;slen=0;
	len = strlen(barnumn);
	sprintf(mfc_str, "%s", " ");
	for(i=0; i<len; i++) {
		value = enc_tablev[i];
		bar = TRUE;
		slen = strlen(value);
		for(j=0; j<slen; j++) {
			if(value[j]=='N' && bar)
				strcat(mfc_str, "1");
			else if(value[j]=='N' && !bar)
				strcat(mfc_str, "0");
			else if(value[j]=='W' && bar)
				strcat(mfc_str, widebar);
			else if(value[j] == 'W' && !bar)
				strcat(mfc_str, widespc);
			bar = !bar;
		}
		strcat(mfc_str, "0");
	}

	// We have to now return mfc_str
	return mfc_str;
}

void gen_barc39(BIM *bi, char *barnum, int scale, char *file, int check_digit) {
	char bars[BIM_MAX_INPUT];
	double total_y;
	int xpos=0,i,j;
	int total_x=0;
	int height,height2,x;
	gdImagePtr im;
	int bgcolor;
	int barcolor;
	int len,slen;
	char val;
	int h;
	char font_arr[] = "";
	int space[4] = {};
	char spaces[] = { "top", "bottom", "left", "right" };

	bars = encode_barc39(bi, barnum, check_digit);
	if(scale<1) scale = 2;
	total_y = (double) scale * bi->height + 10 * scale;
	space = { 2*scale, 2*scale, 2*scale, 2*scale };

	/* Count total width .. */
	xpos = 0;
	xpos = scale * strlen(bars) + 2 * scale * 10;

	/** Allocate the image */
	total_x = xpos + space[2]+space[3];
	xpos = space[2]+scale*10;
	height = floor(total_y-(scale*20));
	height2 = floor(total_y-space[1]);

	im = gdImageCreateTrueColor(total_x, total_y);
	bgcolor = gdImageColorAllocate(im, bi->bgcolor[0], bi->bgcolor[1], bi->bgcolor[2]); // im-ptr, r, g, b
	gdImageFilledRectangle(im, 0, 0, total_x, total_y, bgcolor);
	barcolor = gdImageColorAllocate(im, bi->color[0], bi->color[1], bi->bgcolor[2]);

	len = strlen(bars);
	for(i=0; i<len; i++) {
		h = height;
		val = bars[i];

		if(val=='1')
			gdImageFilledRectangle(im,xpos,space[0],xpos+scale-1,h,barcolor);
		xpos += scale;
	}

	gdImageString(im, gdFontBig, scale*10, 0, barnum);
	// Send to file..
	gdImagePng(im, file);
}
