/**
* @header file for barcode-imag program
* @author VIZCREATIONS
* @copyright (C) 2014
*/

#define BIM_WIDTH 600
#define BIM_HEIGHT 400
#define BIM_MAX_PATH 255
#define BIM_MAX_CHAR 255
#define BIM_MIN_CHAR 50
enum {
	FALSE = -1,
	TRUE = 1
};
#define CGI_MODE FALSE
#define GD_MODE TRUE
#define ROOT "/home/vizcreations/prog/barcode-imag"
#define DS '/'
#define IM_PATH "images"

typedef struct bimag {
	char name[BIM_MIN_CHAR];
	char number[BIM_MIN_CHAR];
	char encoding[BIM_MIN_CHAR];
	char color[BIM_MIN_CHAR];
	char bgcolor[BIM_MIN_CHAR];
	int width;
	int height;
	char font[BIM_MIN_CHAR];
	int scale;
	char format[BIM_MIN_CHAR];
	char path[BIM_MAX_PATH];
} BIM;

char *encode_barc128(BIM *);
void gen_barc128(BIM *);
char *encode_barc39(BIM *);
void gen_barc39(BIM *);

/** Common function */
void gen_barc(BIM *);
void setup_barc(BIM *); // Initialize the struct member data
