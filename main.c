/**
* @main source file for barcode-imag library
* @author VIZCREATIONS
* @copyright (C) 2014
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gd.h>
#include <gdfonts.h>
#include "bimag.h"

int main(int argc, char *argv[]) {
	gdImagePtr im_out, dst_out;
	int im_width=BIM_WIDTH;
	int im_height=BIM_HEIGHT;
	int dst_width, dst_height;
	int bgcolor; // Usually White
	int textcolor; // Usually Black
	char *text = "I am a barcode-program..";

	FILE *fp;
	char *filename;
	filename = (char *)malloc(BIM_MAX_PATH);
	if(filename)
		sprintf(filename, "%s%c%s%c%s", ROOT, DS, IM_PATH, DS,
							"pngimage.png");
	else
		return -1;

	BIM bim;
	im_out = gdImageCreateTrueColor(im_width,im_height); // Set it's resolution up..
	bgcolor = gdImageColorAllocate(im_out, 255, 255, 255); // We can also use HEX 0xFF
	textcolor = gdImageColorAllocate(im_out, 0, 0, 0);

	gdImageString(im_out, gdFontSmall, 10, 10, text, bgcolor);

	fp = fopen(filename, "w");
	if(fp) {
		gdImagePng(im_out, fp); // Will output to stdout => shell
		fclose(fp);
	}
	printf("Image saved successfully in: %s\n", filename);
	gdImageDestroy(im_out);
	free(filename);
	return 0;
}
